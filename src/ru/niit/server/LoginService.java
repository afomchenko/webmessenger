package ru.niit.server;


import ru.niit.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/* сервис авторизации
* запускается в отдельном потоке
* реализует паттерн "одиночка"*/
public class LoginService implements Runnable {
    private static Object lock = new Object();
    private static LoginService loginService;
    Thread t;
    private int port;


    /* конструктор принимает адрес TCP-порта сервера */
    private LoginService(int port) {
        this.port = port;
        t = new Thread(this);
        //t.setDaemon(true);

        t.start();
    }

    /* получение экземпляра */
    public static LoginService getInstance(int port) {
        synchronized (lock) {
            if (loginService == null) {
                loginService = new LoginService(port);
            }
            return loginService;
        }
    }


    @Override
    public void run() {

        MessServer server = MessServer.getInstance();

        ServerSocket ssock;
        try {
            ssock = new ServerSocket(port);
            while (true) {
                Socket sock = ssock.accept();
                BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                PrintWriter writer = new PrintWriter(sock.getOutputStream());
                String name = reader.readLine();

                if (name.startsWith("createuser:")) {
                    String[] userFields = name.split(":");
                    MessServer.getInstance().addUser(userFields[1], userFields[2]);
                    writer.println("ACCEPT");
                    writer.flush();
                    continue;
                } else if (name.startsWith("chpass:")) {
                    String[] passFields = name.split(":");
                    server.changeUserPassword(Integer.parseInt(passFields[1]), passFields[2]);
                    writer.println("CHANGED");
                    writer.flush();
                    continue;
                }

                String password = reader.readLine();
                User tryUser = MessServer.getInstance().getUserByName(name);

                if (tryUser != null && tryUser.authorize(password)) {
                    writer.println("accept:" + tryUser.getId());
                    writer.flush();

                    //System.out.println("old socket:" + server.getSocket(tryUser));
                    if (server.getSocket(tryUser) != null && !server.getSocket(tryUser).equals(sock))
                        server.getSocket(tryUser).close();
                    server.setSocket(tryUser, sock);
                    //System.out.println("new socket:" + server.getSocket(tryUser));

                    tryUser.getOnline(true);
                    new ClientHandler(tryUser);
                    System.out.println("connected: " + tryUser.getName() + sock.getInetAddress());

                } else {
                    writer.println("fail");
                    writer.flush();
                }
            }
        } catch (IOException e) {
        } finally {

        }

    }
}
