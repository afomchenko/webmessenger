package ru.niit.server;

import ru.niit.Message;
import ru.niit.NetworkHelper;
import ru.niit.User;
import ru.niit.UserImpl;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* класс-синглтон реализующий большинство методов сервера и содержащий внутренние состояния сервера */
public class MessServer {
    private static Lock initLock = new ReentrantLock();
    private static MessServer server;
    private final String PROP_FOLDER = "serverprop/";
    private final String PROP_FILE = "server.prop";
    private Lock updateLock = new ReentrantLock();
    private Condition updateCondition = updateLock.newCondition();
    private Map<Integer, User> users;
    private Map<Integer, User> usersOnline;
    private Map<Integer, List<Message>> unreadCache;
    private Map<User, Socket> socketCache;
    private int usersCount;

    private MessServer() {
        users = loadUsers();
        usersOnline = new ConcurrentHashMap<>();
        unreadCache = new ConcurrentHashMap<>();
        socketCache = new ConcurrentHashMap<>();

    }

    /* получение экземпляра ссылки на сервер */
    public static MessServer getInstance() {
        initLock.lock();
        try {
            if (server == null) {
                server = new MessServer();
            }
            return server;
        } finally {
            initLock.unlock();
        }
    }

    /* загрузка списка пользователей из файла */
    private Map<Integer, User> loadUsers() {
        File propFolder = new File(PROP_FOLDER);
        if (!propFolder.exists()) propFolder.mkdir();

        File propServerFile = new File(PROP_FOLDER + PROP_FILE);
        Map<Integer, User> readUsers = new ConcurrentHashMap<>();
        if (propServerFile.exists()) {

            try (BufferedReader reader = new BufferedReader(new FileReader(propServerFile))) {
                usersCount = Integer.parseInt(reader.readLine());
                while (reader.ready()) {
                    User tempUser = UserImpl.uncompress(reader.readLine());
                    readUsers.put(tempUser.getId(), tempUser);
                }
                return readUsers;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return readUsers;
    }

    public void saveUsers() {
        File propFolder = new File(PROP_FOLDER);
        if (!propFolder.exists()) propFolder.mkdir();

        File propServerFile = new File(PROP_FOLDER + PROP_FILE);
        try (PrintWriter writer = new PrintWriter(new FileWriter(propServerFile))) {

            writer.println(usersCount);
            for (User user : users.values()) {
                writer.println(user.compress());

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public User getUserByName(String userName) {
        if (users != null && users.size() > 0)
            for (User user : users.values()) {
                if (user.getName().equals(userName)) return user;
            }
        return null;
    }

    /* добавление нового пользователя */
    public boolean addUser(String name, String password) {
        usersCount++;
        users.put(usersCount, new UserImpl(usersCount, name, password, false));
        saveUsers();
        return true;
    }

    /* получение всех пользователей */
    public Map<Integer, User> getUsers() {
        return users;
    }

    /* установка пользователя онлайн */
    public void setUserOnline(User user) {
        user.getOnline(true);
        usersOnline.put(user.getId(), user);
    }

    /* установка пароля пользователя */
    public void changeUserPassword(int UserID, String password) {
        users.get(UserID).changePassword(password);
        saveUsers();
    }

    /* установка пользователя оффлайн и удаление его из списка онлайн пользователей */
    public void setUserOffline(User user) {
        user.getOnline(false);
        usersOnline.remove(user.getId());
    }

    /* получение всех пользователей онлайн */
    public Map<Integer, User> getUsersOnline() {
        return usersOnline;
    }

    /* получение сокета связаного с пользователем */
    public Socket getSocket(User user) {
        Socket socket = socketCache.get(user);
        if (socket != null && !socket.isClosed()) return socket;
        else return null;
    }

    /* привязка сокета к пользователю и сохранение в кэше */
    public void setSocket(User user, Socket sock) {
        socketCache.put(user, sock);
    }

    /* получение непрочитаных сообщений для пользователя */
    public List<Message> getUnread(int userID) {
        return unreadCache.get(userID);
    }

    /* добавление непрочитанного сообщения */
    public void addUnread(int userID, Message message) {

        if (unreadCache.get(userID) == null) {
            unreadCache.put(userID, new ArrayList<Message>());
        }
        unreadCache.get(userID).add(message);
    }

    /* пересылка всех непрочитаных сообщений и очистка из кэша */
    public void sendUnread(User user) throws IOException {
        List<Message> unreads = getUnread(user.getId());
        if (unreads != null && unreads.size() > 0) {
            Socket sock = socketCache.get(user);

            Iterator<Message> it = unreads.iterator();
            while (it.hasNext()) {
                Message m = it.next();
                NetworkHelper.sendToSocket(sock, m);
                System.out.println(m);
                it.remove();
            }

        }
    }
}
