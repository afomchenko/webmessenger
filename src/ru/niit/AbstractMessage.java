package ru.niit;

/* Здесь находится реализация большинства методов интерфейса Message */
public abstract class AbstractMessage implements Message {
    private int fromID;
    private int toID;
    private String date;
    private MessageContent mess;

    /* строка используется для пердачи сообщения по сети и сохранения в файл */
    @Override
    public String getString() {
        return (fromID + "\u001f" + toID + "\u001f" + mess.getContent().toString() + "\u001f" + date);
    }

    @Override
    public void setMessage(int from, int to, MessageContent mess) {
        this.fromID = from;
        this.toID = to;
        this.mess = mess;
    }

    @Override
    public String toString() {
        return fromID +
                "->" + toID +
                ": " + mess;
    }

    /* получение идентификатора отправителя */
    @Override
    public int getFromID() {
        return fromID;
    }

    /* установка идентификатора отправителя */
    @Override
    public void setFromID(int fromID) {
        this.fromID = fromID;
    }

    /* получение идентификатора получателя */
    @Override
    public int getToID() {
        return toID;
    }

    /* установка идентификатора получателя */
    @Override
    public void setToID(int toID) {
        this.toID = toID;
    }

    /* получение содержимого получателя */
    @Override
    public MessageContent getContent() {
        return mess;
    }

    /* получение даты создания сообщения */
    @Override
    public String getDate() {
        return date;
    }

    /* установка идентификатора получателя
    * используется только внутри реализации*/
    protected void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractMessage)) return false;

        AbstractMessage that = (AbstractMessage) o;

        if (fromID != that.fromID) return false;
        return toID == that.toID && !(mess != null ? !mess.equals(that.mess) : that.mess != null);
    }

    @Override
    public int hashCode() {
        int result = fromID;
        result = 31 * result + toID;
        result = 31 * result + (mess != null ? mess.hashCode() : 0);
        return result;
    }
}
