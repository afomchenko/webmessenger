package ru.niit;

import java.text.SimpleDateFormat;
import java.util.Date;

/* конкретная реализация сообщения */
public class UserMessage extends AbstractMessage {

    public UserMessage(int fromID, int toID, MessageContent content) {
        super.setMessage(fromID, toID, content);

        /* при создании сообщения записывается текущее время создания */
        Date current = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        super.setDate(formatDate.format(current));
    }

    public UserMessage(int fromID, int toID, MessageContent content, String date) {
        super.setMessage(fromID, toID, content);
        super.setDate(date);
    }

    public UserMessage() {
        Date current = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy hh:mm");
        super.setDate(formatDate.format(current));
    }
}
