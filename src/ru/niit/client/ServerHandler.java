package ru.niit.client;


import ru.niit.Message;
import ru.niit.NetworkHelper;

import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;

/* класс-слушатель за входящими сообщениями от сервера
 * запускается в отдельном потоке */
public class ServerHandler implements Runnable {
    protected Thread t;
    private Socket currentSocket;


    public ServerHandler(Socket currentSocket) {
        this.currentSocket = currentSocket;
        t = new Thread(this);
        t.setDaemon(true);
        t.start();
    }

    @Override
    public void run() {
        ClientHelper client = ClientHelper.getInstance();
        ClientMainWindow mainWindow = ClientMainWindow.getInstance();

        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ClientMainWindow mainWindow = ClientMainWindow.getInstance();
                    try {
                        ClientHelper.getInstance().loadHistory();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mainWindow.setCurrentConnected(ClientHelper.getInstance().getCurrentUser());
                }
            }).start();

            while (true) {
                Message mess = null;
                try {
                    mess = NetworkHelper.recieveFromSocket(currentSocket);
                } catch (EOFException e) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
                if (mess != null) {
                    if (mess.getFromID() != 0) {
                        System.out.println(client.getUserFromID(mess.getFromID()) + " >> "
                                + client.getUserFromID(mess.getToID()) + ": "
                                + mess.getContent().toString());
                        client.addInputMessageToCache(mess);
                        if (mainWindow.getCurrentSendTo() != null && mainWindow.getCurrentSendTo().getId() == mess.getFromID())
                            mainWindow.redrawOutput(client.getUserFromID(mess.getFromID()), true);
                    } else if (mess.getContent().getContent() instanceof String) {
                        if (mess.getContent().toString().contains("usersUpdateList/")) {
                            client.addUsers(mess.getContent().toString().replace("usersUpdateList/", ""));
                            mainWindow.updateUserList(client.getUsers().values());

                        } else {
                            System.out.println("server: " + mess.getContent().getContent());
                            if (mainWindow.getCurrentSendTo() != null && mainWindow.getCurrentSendTo().getId() == mess.getToID()) {
                                client.addSystemMessageToCache(mess);
                                mainWindow.redrawOutput(client.getUserFromID(mess.getToID()), true);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
        } finally {
            client.saveHistory();
            try {
                if (currentSocket != null && !currentSocket.isClosed())
                    currentSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

