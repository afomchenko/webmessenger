package ru.niit.client;


import ru.niit.*;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/* основной класс клиента
 * реализует паттерн синглтон */
public class ClientHelper {

    private static final String PROP_FOLDER = "clientprop/";
    private static final String PROP_FILE = "client.prop";
    private static final String HISTORY_FOLDER = "history/";
    private static final Object lock = new Object();
    protected static Socket clientSocket;
    private static ClientHelper instance;
    private User currentUser;
    private Map<Integer, User> users;
    private Map<Integer, List<Message>> messages;
    private Map<Integer, List<Message>> messagesOld;
    private ClientProperties clientProperties;
    private UserProperties userProperties;
    private ClientMainWindow mainWindow;
    private String lastUserPassword;

    private ClientHelper() {
        users = new TreeMap<>();
        users.put(0, new UserImpl(0, "Server", true));
        messages = new ConcurrentHashMap<>();
        messagesOld = new ConcurrentHashMap<>();
        clientProperties = new ClientProperties();

        /* создание служебных папок */
        File propfolder = new File(PROP_FOLDER);
        if (!propfolder.exists())
            propfolder.mkdir();

        File histfolder = new File(HISTORY_FOLDER);
        if (!histfolder.exists())
            histfolder.mkdir();

        if (new File(PROP_FOLDER + PROP_FILE).exists())
            clientProperties = loadClientProperties();
        userProperties = new UserProperties();

        mainWindow = ClientMainWindow.getInstance();

    }

    /* получение экземпляра ссылки на клиент*/
    public static ClientHelper getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new ClientHelper();
            }
            return instance;
        }
    }

    public ClientProperties getClientProperties() {
        return clientProperties;
    }

    public List<Message> getMessages(int userId) {
        return messages.get(userId);
    }

    public void addInputMessageToCache(Message message) {
        if (!messages.containsKey(message.getFromID()))
            messages.put(message.getFromID(), new ArrayList<Message>());
        messages.get(message.getFromID()).add(message);
    }

    public void addSystemMessageToCache(Message message) {
        if (!messages.containsKey(message.getToID()))
            messages.put(message.getToID(), new ArrayList<Message>());
        messages.get(message.getToID()).add(message);
    }

    public void addOutputMessageToCache(Message message) {
        if (!messages.containsKey(message.getToID()))
            messages.put(message.getToID(), new ArrayList<Message>());
        messages.get(message.getToID()).add(message);

    }

    /* запрос смены пароля пользователя*/
    protected boolean changeUserPassword(String password) {
        if (password != null) {
            if (password.length() < 5) return false;

            if (clientSocket != null && !clientSocket.isClosed()) try {
                clientSocket.close();
                System.out.println(clientSocket.isClosed());
            } catch (IOException e) {
                e.printStackTrace();
            }

            Socket chpassSocket = null;
            try {
                chpassSocket = new Socket(clientProperties.getServerIP(), clientProperties.getServerPort());

                PrintWriter writer = new PrintWriter(chpassSocket.getOutputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(chpassSocket.getInputStream()));
                writer.println("chpass:" + currentUser.getId() + ":" + password);
                writer.flush();

                String serverAnswer = reader.readLine();
                if (serverAnswer.equals("CHANGED")) {
                    chpassSocket.close();
                    System.out.println("Tsocket  closed");
                    if (currentUser != null) {
                        if (tryLogin(currentUser.getName(), password))
                            new ServerHandler(ClientHelper.clientSocket);
                    }
                    System.out.println("TRYLOGIN");
                    return true;
                }
            } catch (UnknownHostException e) {
                return false;
            } catch (IOException e) {
                return false;
            } finally {
                try {
                    if (chpassSocket != null && !chpassSocket.isClosed())
                        chpassSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }


    /* запрос создания нового пользователя*/
    protected int createUser(String name, String password) {

        final int ISEXISTS = 1;
        final int ACCEPT = 0;
        final int SHORTNAME = 2;
        final int ILLEGALSYMBOLS = 3;
        final int FAIL = -1;


        if (name != null) {
            if (name.length() < 5 || password.length() < 1) return SHORTNAME;
            if (name.equals("server") || name.startsWith("chpass:") || name.startsWith("createuser:") || name.matches(".*[\\W].*"))
                return ILLEGALSYMBOLS;

            if (clientSocket != null && !clientSocket.isClosed()) try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Socket createUserSocket = null;
            try {
                createUserSocket = new Socket(clientProperties.getServerIP(), clientProperties.getServerPort());
                PrintWriter writer = new PrintWriter(createUserSocket.getOutputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(createUserSocket.getInputStream()));
                writer.println("createuser:" + name + ":" + password);
                writer.flush();

                String serverAnswer = reader.readLine();

                switch (serverAnswer) {
                    case "ACCEPT":
                        createUserSocket.close();
                        if (currentUser != null) {
                            Thread.sleep(500);
                            if (tryLogin(currentUser.getName(), lastUserPassword))
                                new ServerHandler(ClientHelper.clientSocket);
                        }
                        return ACCEPT;
                    case "ISEXISTS":
                        return ISEXISTS;
                    default:
                        return FAIL;
                }
            } catch (UnknownHostException e) {
                return FAIL;
            } catch (IOException e) {
                return FAIL;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return FAIL;
    }

    /* запрос авторизации пользователя*/
    protected boolean tryLogin(String name, String pass) throws IOException {

        try {
            clientSocket = new Socket(clientProperties.getServerIP(), clientProperties.getServerPort());
        } catch (ConnectException e) {
            System.err.println("server offline");
            return false;
        }
        PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        writer.println(name);
        writer.flush();
        writer.println(pass);
        writer.flush();
        String serverAnswer = reader.readLine();
        if (serverAnswer.contains("accept")) {
            if (currentUser != null)
                saveHistory();
            saveClientProperties();
            currentUser = new UserImpl(Integer.parseInt((serverAnswer.split(":"))[1]), name, pass, false);
            lastUserPassword = pass;
            return true;
        } else return false;

    }

    /* получение текущего авторизованного пользователя */
    public User getCurrentUser() {
        return currentUser;
    }

    /* отправка  сообщения */
    public void writeMessage(String str, User recepient) {
        try {

            if (!clientSocket.isClosed()) {
                Message mess = new UserMessage();

                str = str.replaceAll("\n", "<br>");
                mess.setMessage(currentUser.getId(), recepient.getId(), new UserMessageContent(str));
                if (clientSocket.isClosed()) {
                    return;
                }
                NetworkHelper.sendToSocket(clientSocket, mess);

                addOutputMessageToCache(mess);
                if (mainWindow.getCurrentSendTo() != null && mainWindow.getCurrentSendTo().getId() == mess.getToID())
                    mainWindow.redrawOutput(getUserFromID(mess.getToID()), true);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* получение списка всез пользователей */
    public Map<Integer, User> getUsers() {
        return users;
    }

    /* добавление нового пользователя список контактов */
    public void addUsers(String userList) {
        for (String user : userList.split("/")) {
            String[] s = user.split(":");
            if (s[0].endsWith("-offline"))
                users.put(Integer.parseInt(s[1]), new UserImpl(Integer.parseInt(s[1]), s[0].replace("-offline", ""), false));
            else
                users.put(Integer.parseInt(s[1]), new UserImpl(Integer.parseInt(s[1]), s[0], true));
        }
    }

    /* получение пользователа по идентификатору */
    public User getUserFromID(int id) {
        return users.get(id);
    }

    /* получение заголовка сообщения с именем отправителя и датой */
    public String getMessageStamp(Message mess) {
        if (mess.getFromID() == 0) {
            return "Server:&nbsp;";
        } else {
            return getUserFromID(mess.getFromID()) +
                    " <span color = \"red\">&nbsp;(" +
                    mess.getDate() +
                    "):</span><br>";
        }
    }

    /*  загрузка общих настроек */
    private ClientProperties loadClientProperties() {
        try (ObjectInput propInput = new ObjectInputStream(new FileInputStream(PROP_FOLDER + PROP_FILE))) {
            return (ClientProperties) propInput.readObject();
        } catch (Exception e) {
            System.err.println("properties load error: " + e);
        }
        return null;
    }

    /* получение созраненнных сообщений из истроии полученных в предыдущих сессиях */
    public List<Message> getOldMessages(int userId) {
        return messagesOld.get(userId);
    }

    /* загрузка пользовательских настроек */
    public boolean loadUserProperties(User user) {
        File file = new File(PROP_FOLDER + user.getId() + ".prop");
        if (file.exists())

            try (ObjectInput propInput = new ObjectInputStream(new FileInputStream(file))) {

                userProperties = (UserProperties) propInput.readObject();
                return true;
            } catch (Exception e) {
                System.err.println("properties load error: " + e);
            }
        return false;
    }

    /* сохранение пользовательских настроек */
    public void saveUserProperties() {
        try (ObjectOutput propOutput = new ObjectOutputStream(new FileOutputStream(PROP_FOLDER + userProperties.getUser() + ".prop"))) {
            propOutput.writeObject(userProperties);
        } catch (Exception e) {
            System.err.println("properties save error: " + e);
        }
    }

    /* сохранение общих настроек */
    public void saveClientProperties() {
        try (ObjectOutput propOutput = new ObjectOutputStream(new FileOutputStream(PROP_FOLDER + PROP_FILE))) {
            propOutput.writeObject(clientProperties);
        } catch (Exception e) {
            System.err.println("properties save error: " + e);
        }
    }

    /* получение пользовательских настроек */
    public UserProperties getUserProperties() {
        return userProperties;
    }

    /* созранение истории сообщений в файл */
    public void saveHistory() {
        File userHistoryFolder = new File(HISTORY_FOLDER + currentUser.getId() + "history/");
        if (!userHistoryFolder.exists())
            userHistoryFolder.mkdir();

        for (Integer id : messages.keySet()) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(HISTORY_FOLDER + currentUser.getId() + "history/" + id, true))) {

                for (Message message : messages.get(id)) {
                    if (message.getFromID() == 0) continue;
                    writer.write(message.getString());
                    writer.write("\r\n");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        messages.clear();
        messagesOld.clear();
    }

    /* получение последних сообщений из истории */
    public void loadHistory() throws IOException {
        File userHistoryFolder = new File(HISTORY_FOLDER + currentUser.getId() + "history/");
        try {
            if (userHistoryFolder.exists()) {
                for (File id : userHistoryFolder.listFiles()) {
                    BufferedReader reader = new BufferedReader(new FileReader(id));
                    messagesOld.put(Integer.parseInt(id.getName()), new ArrayList<Message>());
                    //for (int i = 0; i < 10; i++) {
                    while (reader.ready()) {
                        String[] messRaw = reader.readLine().split("\u001f");
                        messagesOld.get(Integer.parseInt(id.getName())).add(new UserMessage(Integer.parseInt(messRaw[0]),
                                Integer.parseInt(messRaw[1]),
                                new UserMessageContent(messRaw[2]),
                                messRaw[3]));
                    }
                    //}
                }
            }
        } catch (NullPointerException e) {
            System.err.println("cannot read history");
        }
        for (Integer integer : messagesOld.keySet()) {
            for (Message message : messagesOld.get(integer)) {
            }
        }


    }
}

/* класс общих настроек клиента*/
class ClientProperties implements Serializable {
    private String serverIP;
    private int serverPort;

    public ClientProperties() {
        serverIP = "127.0.0.1";
        serverPort = 8888;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        Pattern checkIPattern = Pattern.compile(
                "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

        if (serverIP != null && serverIP.matches(checkIPattern.toString()))
            this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        if (serverPort > 0 && serverPort < 65535)
            this.serverPort = serverPort;
    }
}

/* класс пользовательских настроек */
class UserProperties implements Serializable {
    private int user;
    private int lastContactID;

    public int getLastContactID() {
        return lastContactID;
    }

    public void setLastContactID(int lastContactID) {
        this.lastContactID = lastContactID;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}