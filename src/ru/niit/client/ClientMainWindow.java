package ru.niit.client;

import ru.niit.Message;
import ru.niit.User;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* Синглтонный класс с реализацией основного окна */
public class ClientMainWindow extends JFrame {
    private static ClientMainWindow instance = null;
    private static Lock lock = new ReentrantLock();
    private static Lock updateLock = new ReentrantLock();
    private static Condition updateCondition = updateLock.newCondition();
    private final String htmlHeadFirst = "<html><head><style type=\"text/css\">";
    private final String htmlStyleTo = ".to { background: #EAEAEA;}";
    private final String htmlStyleFrom = ".from { background: #8BACBD; }";
    private final String htmlStyleSys = ".sys { background: #fa9581; }";
    private final String htmlHeadSecond = "</style></head><body>";
    private final String htmlFoot = "<br></body></html>";
    private JButton settingsButton;
    private JButton newUserButton;
    private JList contactList;
    private JButton sendButton;
    private JTextArea messageInputField;
    private JLabel currentConnected;
    private JPanel mainWindow;
    private JTextPane messageOutputList;
    private JScrollPane contentScrollPane;
    private User currentSendTo;

    private ClientMainWindow() {


        this.setContentPane(mainWindow);

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (messageInputField.getText().length() == 0) return;
                ClientHelper.getInstance().writeMessage(messageInputField.getText(), currentSendTo);
                messageInputField.setText("");
            }
        });
        newUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new LogIn();
            }
        });
        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SettingsDialog();
            }
        });
        contactList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting())
                    setCurrentSendTo((User) contactList.getSelectedValue());
            }
        });
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                ClientHelper client = ClientHelper.getInstance();
                client.saveUserProperties();
                client.saveClientProperties();
                if (client.getCurrentUser() != null)
                    client.saveHistory();
                System.exit(0);
            }
        });
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static ClientMainWindow getInstance() {
        try {
            lock.lock();
            if (instance == null) {
                instance = new ClientMainWindow();
            }
            return instance;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        ClientMainWindow.getInstance();
    }

    public User getCurrentSendTo() {
        return currentSendTo;
    }

    /* установка текущего получателя */
    public void setCurrentSendTo(User contact) {
        if (contact == null) return;
        currentSendTo = contact;
        messageOutputList.setText(buildMessageList(contact));
        messageOutputList.setCaretPosition(messageOutputList.getDocument().getLength());

        ClientHelper.getInstance().getUserProperties().setLastContactID(contact.getId());
    }

    /* обновление списка контактов */
    public void updateUserList(Collection<User> userList) {
        updateLock.lock();
        try {
            ClientHelper client = ClientHelper.getInstance();
            List<User> list = new ArrayList<>(userList);
            Collections.sort(list);
            DefaultListModel<User> model = new DefaultListModel<>();

            for (User user : list) {
                if (!user.equals(client.getCurrentUser()) && user.getId() != 0)
                    model.addElement(user);
            }

            contactList.setModel(model);
            if (currentSendTo != null) {
                setCurrentSendTo(currentSendTo);
                contactList.setSelectedValue(currentSendTo, true);

            }
        } finally {
            updateCondition.signal();
            updateLock.unlock();

        }
    }

    /* установка текущенго пользователя отправителя */
    public void setCurrentConnected(User user) {

        updateLock.lock();
        try {
            while (contactList.getModel().getSize() == 0) {
                updateCondition.await();
            }

            ClientHelper client = ClientHelper.getInstance();
            if (ClientHelper.getInstance().loadUserProperties(user)) {
                contactList.setSelectedValue(client.getUserFromID(client.getUserProperties().getLastContactID()), true);
                setCurrentSendTo((User) contactList.getSelectedValue());
            } else {
                client.getUserProperties().setUser(user.getId());
            }
            currentConnected.setText("User: " + user.getName());
            sendButton.setEnabled(true);


        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            updateLock.unlock();
        }
    }

    public void redrawOutput(User contact, boolean scrollDown) {
        messageOutputList.setText(buildMessageList(contact));

        if (scrollDown)
            messageOutputList.setCaretPosition(messageOutputList.getDocument().getLength());
    }


    /* генерация списка сообщений
     * использует HTML3, каждое сообщение оформляется в виде отдельной таблицы */
    public String buildMessageList(User contact) {

        ClientHelper client = ClientHelper.getInstance();
        if (contact == null || (client.getMessages(contact.getId()) == null &&
                client.getOldMessages(contact.getId()) == null)) return "no messages";
        try {
            updateLock.lock();
            StringBuilder messageWindowText = new StringBuilder();

            messageWindowText.append(htmlHeadFirst).append(htmlStyleTo).append(htmlStyleFrom).append(htmlStyleSys).append(htmlHeadSecond);

             /* для сообщений из предыдущей сессии */
            if (client.getOldMessages(contact.getId()) != null)
                for (Message message : client.getOldMessages(contact.getId())) {
                    if (message.getFromID() == contact.getId()) {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td width=10%>&nbsp;</td><td class = \"to\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td></tr></table>");
                    } else if (message.getFromID() == 0) {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td width=5%>&nbsp;</td><td class = \"sys\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td><td width=5%>&nbsp;</td></tr></table>");
                    } else {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td class = \"from\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td><td width=10%>&nbsp;</td></tr></table>");
                    }
                }
             /* для сообщений из текущей сессии*/
            if (client.getMessages(contact.getId()) != null)
                for (Message message : client.getMessages(contact.getId())) {
                    if (message.getFromID() == contact.getId()) {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td width=10%>&nbsp;</td><td class = \"to\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td></tr></table>");
                    } else if (message.getFromID() == 0) {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td width=5%>&nbsp;</td><td class = \"sys\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td><td width=5%>&nbsp;</td></tr></table>");
                    } else {
                        messageWindowText.append("<table width=\"100%\" border=\"0\" cellpadding=\"5\"><tr><td class = \"from\"><strong>")
                                .append(client.getMessageStamp(message))
                                .append("</strong>")
                                .append(message.getContent().toString())
                                .append("</td><td width=10%>&nbsp;</td></tr></table>");
                    }
                }
            messageWindowText.append(htmlFoot);
            //contentScrollPane.getVerticalScrollBar().setValue(contentScrollPane.getHeight());

            return messageWindowText.toString();

        } finally {
            updateLock.unlock();
        }
    }

}
