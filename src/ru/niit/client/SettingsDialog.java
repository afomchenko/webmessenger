package ru.niit.client;

import javax.swing.*;
import java.awt.event.*;
import java.util.Arrays;

/* диалог установки настроек соединения*/
public class SettingsDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTabbedPane tabsSettings;
    private JTextField newUserNameField;
    private JTextField ipField;
    private JTextField tcpField;
    private JPasswordField newUserPassField1;
    private JPasswordField newPassField1;
    private JPasswordField newPassField2;
    private JPasswordField newUserPassField2;
    private JPanel serverSettings;
    private JPanel addNewUserSettings;
    private JPanel changePassSettings;
    private JLabel currentLogged;
    private JButton buttonApply;


    public SettingsDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        buttonApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                apply();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        ipField.setText(ClientHelper.getInstance().getClientProperties().getServerIP());
        tcpField.setText("" + ClientHelper.getInstance().getClientProperties().getServerPort());

        if (ClientHelper.getInstance().getCurrentUser() != null)
            currentLogged.setText("Change password: " + ClientHelper.getInstance().getCurrentUser().getName());
        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);

    }


    private void apply() {
        ClientHelper client = ClientHelper.getInstance();
        if (tabsSettings.getSelectedIndex() == 0) {
            System.out.println("00000");

            client.getClientProperties().setServerIP(ipField.getText());
            try {
                client.getClientProperties().setServerPort(Integer.parseInt(tcpField.getText()));
            } catch (NumberFormatException ignored) {
            }

        } else if (tabsSettings.getSelectedIndex() == 1) {

            if (Arrays.equals(newUserPassField1.getPassword(), newUserPassField2.getPassword()))
                switch (client.createUser(newUserNameField.getText(), new String(newUserPassField1.getPassword()))) {
                    case 0:
                        new SystemInfoDialog("Created new user:" + newUserNameField.getText());
                        newUserNameField.setText("");
                        newUserPassField1.setText("");
                        newUserPassField2.setText("");
                        break;
                    case 1:
                        new SystemInfoDialog("Error: user " + newUserNameField.getText() + "is exists");
                        break;
                    case 2:
                        new SystemInfoDialog("Error: username must contains 5 characters");
                        break;
                    case 3:
                        new SystemInfoDialog("Error: username contains illegal characters");
                        break;
                    default:
                        new SystemInfoDialog("Creating new user failed");
                        break;
                }
            else new SystemInfoDialog("Error: password fields must be equal");


        } else if (tabsSettings.getSelectedIndex() == 2) {
            if (Arrays.equals(newPassField1.getPassword(), newPassField2.getPassword())) {
                if (client.changeUserPassword(new String(newPassField1.getPassword()))) {
                    new SystemInfoDialog("Password changed");
                    newPassField1.setText("");
                    newPassField2.setText("");
                } else new SystemInfoDialog("Error: password don't changed");
            } else new SystemInfoDialog("Error: password fields must be equal");

        }


    }


    private void onCancel() {
        dispose();
    }

}
