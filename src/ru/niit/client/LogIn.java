package ru.niit.client;

import ru.niit.User;

import javax.swing.*;
import java.io.IOException;

/* диалог авторизации */
public class LogIn extends javax.swing.JDialog {
    private javax.swing.JPanel contentPane;
    private javax.swing.JButton buttonOK;
    private javax.swing.JButton buttonCancel;
    private JTextField nameField;
    private JPasswordField passwordField;

    public LogIn() {

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        }, javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0), javax.swing.JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        this.pack();
        setLocationRelativeTo(null);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        LogIn dialog = new LogIn();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void onOK() {
        User currentUser = ClientHelper.getInstance().getCurrentUser();
        //if(currentUser==null || !nameField.getText().equals(currentUser.getName()))
        try {
            if (ClientHelper.getInstance().tryLogin(nameField.getText(), new String(passwordField.getPassword()))) {
                new ServerHandler(ClientHelper.clientSocket);
            }
            else
                new SystemInfoDialog("Log In failed");
        } catch (IOException e) {
            e.printStackTrace();
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
