package ru.niit;

import java.io.Serializable;

/* Шаблонный иртерфейс для содержимого сообщения*/
public interface MessageContent <T> extends Serializable{
    T getContent();
    void setContent(T content);
}
